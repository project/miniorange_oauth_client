<?php

namespace Drupal\miniorange_oauth_client\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\miniorange_oauth_client\Utilities;
use Drupal\Core\Url;

class ConfirmDelete extends FormBase
{

  /**
   * {@inheritDoc}
   */
  public function getFormId()
  {
    return 'miniorange_oauth_client_configure_app';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {

    $form['markup_library'] = [
      '#attached' => [
        'library' => [
          "miniorange_oauth_client/miniorange_oauth_client.oauth_config",
          "miniorange_oauth_client/miniorange_oauth_client.admin",
          "miniorange_oauth_client/miniorange_oauth_client.style_settings",
          "core/drupal.dialog.ajax",
        ],
      ],
    ];
    $action = \Drupal::request()->query->get('action');

    $client_app = \Drupal::config('miniorange_oauth_client.settings')->get('miniorange_oauth_client_app');

    $form['confirm_fieldset'] = [
      '#type' => 'fieldset'
    ];

    $message = $this->t('Are you sure you want to @action the configuration for @app?', ['@action' => $action, '@app' => $client_app]);

    $form['confirm_fieldset']['message'] = [
      '#markup' => $message,
      '#suffix' => '<br>',
    ];

    $form['confirm_fieldset'][$action] = [
      '#type' => 'link',
      '#title' => $action === 'delete' ? $this->t('Delete') : $this->t('Reset'),
      '#url' => Url::fromRoute('miniorange_oauth_client.delete_config'),
      '#attributes' => ['class' => ['button', 'button--primary']],
    ];

    $form['confirm_fieldset']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('miniorange_oauth_client.config_clc')->setOption('query', ['app_name' => $client_app] + ($action === 'delete' ? [] : ['action' => 'update'])),
      '#attributes' => [
        'class' => ['button', 'cancel-button'],
      ],
    ];

    Utilities::moOAuthShowCustomerSupportIcon($form, $form_state);
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
  }

}
