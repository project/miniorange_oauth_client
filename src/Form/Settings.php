<?php

namespace Drupal\miniorange_oauth_client\Form;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\miniorange_oauth_client\Utilities;
use Drupal\Core\Url;

/**
 * Class for handling signin settings tab.
 */
class Settings extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'miniorange_oauth_client_settings';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $base_url = \Drupal::request()->getSchemeAndHttpHost().\Drupal::request()->getBasePath();
    $url_path = $base_url . '/' . \Drupal::service('extension.list.module')->getPath('miniorange_oauth_client') . '/includes/images';
    $attachments['#attached']['library'][] = 'miniorange_oauth_client/miniorange_oauth_client.admin';
    $form['markup_library'] = [
         '#attached' => [
                'library' => [
                    "miniorange_oauth_client/miniorange_oauth_client.admin",
                    "miniorange_oauth_client/miniorange_oauth_client.style_settings",
                    "miniorange_oauth_client/miniorange_oauth_client.mo_tooltip",
                    "core/drupal.dialog.ajax"
                ]
            ],
    ];

    $form['header_top_style_1'] = ['#markup' => '<div class="mo_oauth_table_layout_1">'];
    $form['markup_top'] = [
      '#markup' => '<div class="mo_oauth_table_layout mo_oauth_container_signinsettings">',
    ];
    $form['markup_custom_troubleshoot'] = [
      '#type' => 'fieldset',
      '#title' => t('Debugging & Troubleshoot'),
    ];
    $form['markup_custom_troubleshoot']['miniorange_oauth_client_enable_logging'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable Logging'),
      '#default_value' => \Drupal::config('miniorange_oauth_client.settings')->get('miniorange_oauth_client_enable_logging'),
      '#description' => t('Enabling this checkbox will add loggers under the ').'<a href="' . Url::fromRoute('dblog.overview', ['type' => 'miniorange_oauth_client'])->toString().'" target="_blank">'.t('Reports').'</a>'.t(' section'),
      '#prefix' => '<hr>',
    ];
    $form['markup_custom_troubleshoot']['miniorange_oauth_client_siginin1'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => t('Save Configuration'),
      '#submit' => ['::troubleshootsubmitForm'],

    ];

    $form['markup_custom_troubleshoot']['miniorange_oauth_client_enable_logging_download'] = [
      '#type' => 'submit',
      '#value' => t('Download Module Logs'),
      '#limit_validation_errors' => [],
      '#states' => [
        'disabled' => [
          ':input[name="miniorange_oauth_client_enable_logging"]' => ['checked' => FALSE],
        ],
      ],
      '#submit' => ['::miniorangeModuleLogs'],
    ];

    $form['markup_custom_auto_create_user'] = [
      '#type' => 'details',
      '#title' => t('Auto Create Users '). Utilities::getTooltipIcon('', t('Available in the Standard, Premium and Enterprise version'), '<a class= "licensing" href="licensing"><img class = "mo_oauth_pro_icon1" src="' . $url_path . '/pro.png" alt="Premium and Enterprise"></a>', 'mo_oauth_pro_icon_tooltip'),
      '#open' => true
    ];

    $form['markup_custom_auto_create_user']['miniorange_oauth_disable_autocreate_users'] = [
      '#type' => 'checkbox',
      '#title' => t('Check this option if you want to enable ').'<b>'.t('auto creation').'</b>'.t(' of users if user does not exist.'),
      '#disabled' => TRUE,
      '#description'=> t('This feature provides you with an option to automatically create a user if the user is not already present in Drupal.'),
    ];

    $form['markup_custom_signin'] = [
      '#type' => 'details',
      '#title' => t('Page Restriction '). Utilities::getTooltipIcon('', t('Available in the Premium and Enterprise version'), '<a class= "licensing" href="licensing"><img class = "mo_oauth_pro_icon1" src="' . $url_path . '/pro.png" alt="Premium and Enterprise"></a>', 'mo_oauth_pro_icon_tooltip'). '<a class="mo_oauth_client_how_to_setup" style="float:right;"href="https://developers.miniorange.com/docs/oauth-drupal/sign-in-settings#domain-restriction" target="_blank">'.t('[What is Page restriction and How to Set up]').'</a>',
      '#open' => true,
    ];

    $form['markup_custom_signin']['miniorange_oauth_client_force_auth'] = [
      '#type' => 'checkbox',
      '#title' => t('Protect website against anonymous access '),
      '#disabled' => TRUE,
      '#description' => '<b>'.t('Note: ').'</b>'.t('Users will be redirected to your OAuth server for login in case user is not logged in and tries to access website.').'<br><br>',
    ];

    $form['markup_custom_signin']['miniorange_oauth_set_of_page_restriction'] = array(
      '#type' => 'fieldset',
    );

    $types_of_page_restrictions = ['whitelist_pages' => t('Pages to exclude from restriction'),'restrict_pages' => t('Pages to be restricted')];

    $form['markup_custom_signin']['miniorange_oauth_set_of_page_restriction']['choose_type_of_page_restriction'] = array(
      '#type' => 'radios',
      '#options' => $types_of_page_restrictions,
      '#attributes' => array('class' => array('container-inline'),),
      '#default_value' => 'whitelist_pages'
    );

    $form['markup_custom_signin']['miniorange_oauth_set_of_page_restriction']['markup_desc_page_restriction'] = array(
      '#markup' => '<p>'.t('Enter the ').'<b>'.t('line seperated relative URLs').'</b>'.t('. For instace, If the site url is ').'<b>https://www.xyz.com/yyy</b> '.t('then the relative URL would be ').'<b>/yyy</b>.
      <ul>
        <li>'.t('If you want to restrict/allow access to particular ').'<b>\'/abc/pqr/xyz\'</b> '.t('route then use ').'<b>\'/abc/pqr/xyz\'</b>. </li>
        <li>'.t('You also have the option to use the ').'<b>\'*\'</b> '.t('wildcard in URLs to manage page access. For instance, to restrict/allow access to all routes under ').'<b>\'/abc\'</b>, '.t('use the wildcard URL ').'<b>\'/abc/*\'</b>.</li>
      </ul></p>',
    );


    $form['markup_custom_signin']['miniorange_oauth_set_of_page_restriction']['miniorange_oauth_page_whitelist_urls'] = array(
      '#type' => 'textarea',
      '#title' => t('Pages to exclude from auto-redirect to OAuth Provider for anonymous users'),
      '#attributes' => array('style' => 'width:640px;height:80px; background-color: hsla(0,0%,0%,0.08) !important',
      'placeholder' => t('Enter the list of semicolon separated relative URLs of your pages in the textarea.')),
      '#description' => '<b>'.t('Note:').'&nbsp;</b>'.t('Users can access these pages anonymously.').'<b>&nbsp;'.t('Keep this textarea empty if you want to restrict all pages of your site').'</b>',
      '#disabled' => true,
      '#resizable' => FALSE,
      '#states' => array(
        // Only show this field when the checkbox is enabled.
        'visible' => array(
            ':input[name="choose_type_of_page_restriction"]' => array('value' => 'whitelist_pages'),),
       ),
      '#suffix' => '<br>',
    );

    $form['markup_custom_signin']['miniorange_oauth_set_of_page_restriction']['miniorange_oauth_page_restrict_urls'] = array(
      '#type' => 'textarea',
      '#title' => t('Pagess to be restricted'),
      '#attributes' => array('style' => 'width:640px;height:80px; background-color: hsla(0,0%,0%,0.08) !important',
      'placeholder' => t('Enter the list of semicolon separated relative URLs of your pages in the textarea.')),
      '#description' => '<b>'.t('Note:').'&nbsp;</b>'.t('Users will be redirected to your OAuth Server for login when the restricted page is accessed.').'<b>&nbsp;'.t('Only these pages will be restricted and all other pages can be accessed anonymously').'</b>',
      '#disabled' => true,
      '#resizable' => FALSE,
      '#states' => array(
        // Only show this field when the checkbox is enabled.
        'visible' => array(
            ':input[name="choose_type_of_page_restriction"]' => array('value' => 'restrict_pages'),),
       ),
      '#suffix' => '<br>',
    );

    $form['markup_custom_signin']['miniorange_oauth_auto_redirect'] = [
      '#type' => 'checkbox',
      '#title' => t('Check this option if you want to ').'<b>'.t('Replace Drupal login form with IDP/OAuth Provider login from ').'</b>',
      '#disabled' => TRUE,
      '#description' => '<b>'.t('Note:').' </b>'.t('Users will be redirected to your OAuth server for login when the login page is accessed.').'<br><br>',
    ];

    $form['markup_custom_signin']['miniorange_oauth_enable_backdoor'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable ').'<b>'.t('backdoor login ').'</b>',
      '#disabled' => TRUE,
      '#description' => '<b>'.t('Note:').'</b> '.t('Checking this option creates a backdoor to login to your Website using Drupal credentials.'),
    ];

    $form['markup_custom_signin1'] = [
      '#type' => 'details',
      '#title' => t('Domain Restriction ').Utilities::getTooltipIcon('', t('Available in the Enterprise version'), '<a class= "licensing" href="licensing"><img class = "mo_oauth_pro_icon1" src="' . $url_path . '/pro.png" alt="Premium and Enterprise"></a>', 'mo_oauth_pro_icon_tooltip').'<a class="mo_oauth_client_how_to_setup" style="float:right;"href="https://developers.miniorange.com/docs/oauth-drupal/sign-in-settings#domain-restriction" target="_blank">'.t('[What is Domain restriction and How to Set up]').'</a>',
      '#open' => true,
    ];

    $form['markup_custom_signin1']['miniorange_oauth_domain_restriction_checkbox'] = array(
      '#type' => 'checkbox',
      '#title' => t('Check this option if you want ').'<b>'.t('Domain Restriction').'</b>',
      '#disabled' => TRUE,

    );

    $form['markup_custom_signin1']['miniorange_oauth_set_of_radiobuttons'] = array(
      '#type' => 'fieldset',
    );

    $form['markup_custom_signin1']['miniorange_oauth_set_of_radiobuttons']['miniorange_oauth_allow_or_block_domains']=array(
      '#type'=>'radios',
      '#maxlength' => 5,
      '#options' => array('allowed' => t('I want to allow only some of the domains'),'block' => t('I want to block some of the domains')),
      '#disabled' => true,
    );

    $form['markup_custom_signin1']['miniorange_oauth_set_of_radiobuttons']['miniorange_oauth_domains'] = array(
      '#type' => 'textarea',
      '#title' => t('Enter list of domains'),
      '#attributes' => array('style' => 'width:580px;height:80px; background-color: hsla(0,0%,0%,0.08) !important',
      'placeholder' => t('Enter semicolon(;) separated domains (Eg. xxxx.com; xxxx.com)')),
      '#disabled' => true,
      '#suffix' => '<br>',
    );


    $form['markup_custom_login_logout'] = [
      '#type' => 'details',
      '#open' => true,
      '#title' => t('Redirection after SSO Login and Logout '). Utilities::getTooltipIcon('', t('Available in the Standard, Premium and Enterprise version'), '<a class= "licensing" href="licensing"><img class = "mo_oauth_pro_icon1" src="' . $url_path . '/pro.png" alt="Premium and Enterprise"></a>', 'mo_oauth_pro_icon_tooltip'),
    ];

    $form['markup_custom_login_logout']['custom_redirect'] = [
      '#markup' => '<p>'.t('This feature provides you with an option to redirect the users to a default page after Login and Logout').'</p>',
    ];

    $form['markup_custom_login_logout']['miniorange_oauth_client_login_url'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => t('Redirect URL after login'),
      '#attributes' => ['placeholder' => t('Enter complete URL')],
    ];

    $form['markup_custom_login_logout']['after_logout_keep_user_on_same_page'] = [
      '#type' => 'checkbox',
      '#disabled' => true,
      '#title' => t('Keep users on the same page after logout'),
    ];

    $form['markup_custom_login_logout']['miniorange_oauth_client_logout_url'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => t('Redirect URL after logout'),
      '#attributes' => ['placeholder' => t('Enter complete URL')],
      '#description' => t('Please provide the End Session Endpoint of the Identity Provider if you want to enable users to log out from the Identity Provider when they log out from the Drupal site.')
    ];

    $form['markup_custom_login_logout']['end_session_endpoint'] = [
      '#type' => 'checkbox',
      '#disabled' => true,
      '#title' => t('Enable the checkbox if the IDP/OAuth Server enforces id_token in the logout URL'),
    ];

    $form['miniorange_oauth_client_siginin'] = [

      '#type' => 'button',
      '#value' => t('Save Configuration'),
      '#button_type' => 'primary',
      '#disabled' => TRUE,
      '#attributes' => ['style' => '	margin: auto; display:block; '],
    ];

    $form['mo_header_style_end'] = ['#markup' => '</div>'];
    Utilities::moOauthShowCustomerSupportIcon($form, $form_state);
    return $form;
  }

  /**
   * Submit handler for updating base url of site.
   *
   * @param array $form
   *   The form elements array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The formstate.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Enables logging.
   *
   * @param array $form
   *   The form elements array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The formstate.
   */
  public function troubleshootsubmitForm(array &$form, FormStateInterface $form_state) {
    $form_values = $form_state->getValues();
    $enable_logs = $form_values['miniorange_oauth_client_enable_logging'];
    \Drupal::configFactory()->getEditable('miniorange_oauth_client.settings')->set('miniorange_oauth_client_enable_logging', $enable_logs)->save();
    \Drupal::messenger()->addMessage(t('Configurations saved successfully.'));
  }

  /**
   * Filters string provided.
   *
   * @param string $str
   *   The string to be filtered.
   */
  public static function mofilterData(&$str) {
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "\\n", $str);
    if (strstr($str, '"')) {
      $str = '"' . str_replace('"', '""', $str) . '"';
    }
  }

  /**
   * Exports module logs.
   */
  public static function miniorangeModuleLogs() {

    $connection = \Drupal::database();

    // Excel file name for download.
    $fileName = "drupal_oauth_client_loggers_" . date('Y-m-d') . ".xls";

    // Column names.
    $fields = ['WID', 'UID', 'TYPE', 'MESSAGE', 'VARIABLES', 'SEVERITY', 'LINK', 'LOCATION', 'REFERER', 'HOSTNAME', 'TIMESTAMP'];

    // Display column names as first row.
    $excelData = implode("\t", array_values($fields)) . "\n\n";

    // Fetch records from database.
    $query = $connection->query("SELECT * from {watchdog} WHERE type = 'miniorange_oauth_client' OR severity = 3")->fetchAll();

    foreach ($query as $row) {
      $lineData = [$row->wid, $row->uid, $row->type, $row->message, $row->variables, $row->severity, $row->link, $row->location, $row->referer, $row->hostname, $row->timestamp];
      array_walk($lineData, static function(&$value) {
        self::mofilterData($value);
      });

      $excelData .= implode("\t", array_values($lineData)) . "\n";
    }

    // Headers for download.
    header("Content-Type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=\"$fileName\"");

    // Render excel data.
    echo $excelData;
    exit;
  }

}
